# ![La Salle BES](http://jcarreras.es/images/lasalle.png)

# Descripción
-----------------------

Comprende como crear y usar imágenes de distintos tamaños y el impacto en reducción de tamaño que supone.


# Instalación
-----------------------

```
$ make up
```


# Instrucciones
-----------------------

- Entra en `http://localhost` con el navegador Google Chrome
- Abre las DevTools, ve a la parte de `Networking`, filtra por imágenes
- Recarga la página mirando las cabeceras enviadas para cada tipo de imagen
- Recargando diferentes veces la página, haciendo un `Hard Refresh`, también
- Activa el throttling de ‘Good 3G’ y vuelve a probar la página


- Entra en el container con `make bash`
- Entra en http://localhost en tu navegador
- Podrás ver una misma imagen en tamaños diferentes y, abajo, otras imágenes que no se han podido cargar.
- Abre el fichero `src/resize.php` verás que hay el código para cambiar a diferentes tamaños las imágenes, aunque hay una parte del código que falta. Complétala basándote en la documentación del proyecto `https://github.com/eventviva/php-image-resize`
- Ejecuta el fichero haciendo `php resize.php`
•	Entra otra vez en `http://localhost/` para ver si las imágenes se han generado correctamente. Deberían tener los mismos tamaños que las que habría de muestra.



# Desinstalación
-----------------------

```
$ make down
```
