<?php

require('vendor/autoload.php');

use Gumlet\ImageResize;




function resizeImageToFormats($original_path, array $formats) {
  foreach($formats as $format_name => $format_size) {
    $destination_path = str_replace('original', $format_name, $original_path);
    resizeImageToFormat($original_path, $destination_path, $format_size);
  }
}


function resizeImageToFormat($original_path, $destination_path, $format_size) {
  $image = new ImageResize($original_path);
  // ADD LINES HERE TO COMPLETE THE EXERCICE

  // /ADD LINES HERE TO COMPLETE THE EXERCICE
  $image->save($destination_path);
}

$formats = [
  'big' => 800,
  'medium' => 500,
  'small' => 200,
  'thumbnail' => 50,
];


$pending_images = [
  'img/wallpaper2/original.jpg',
  'img/wallpaper3/original.jpg',
];

foreach($pending_images as $pending_image) {
  resizeImageToFormats($pending_image, $formats);  
}